extern crate serde;

use chrono::Utc;
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Block {
    index: u32,
    // keep timestamp in rfc2822
    timestamp: String,
    proof: u32,
    previous_hash: String,
}

pub struct Blockchain {
    chain: Vec<Block>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct MinedResponse {
    message: String,
    block: Block,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ChainResponse {
    chain: Vec<Block>,
    length: usize,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ValidateResponse {
    validation: bool,
}

impl Block {
    pub fn new(index: u32, timestamp: String, proof: u32, previous_hash: String) -> Self {
        Block {
            index,
            timestamp,
            proof,
            previous_hash,
        }
    }

    pub fn index(&self) -> u32 {
        self.index
    }

    pub fn timestamp(&self) -> String {
        self.timestamp.clone()
    }

    pub fn proof(&self) -> u32 {
        self.proof
    }

    pub fn previous_hash(&self) -> String {
        self.previous_hash.clone()
    }
}

impl Blockchain {
    pub fn new() -> Blockchain {
        Blockchain {
            chain: vec![
                // genesis block
                Block::new(0, Utc::now().to_rfc2822(), 1, 0.to_string()),
            ],
        }
    }

    pub fn chain(&self) -> Vec<Block> {
        self.chain.clone()
    }

    pub fn ref_chain<'a>(&'a self) -> &'a Vec<Block> {
        &self.chain
    }

    pub fn create_block(&mut self, proof: u32, previous_hash: &String) -> &Block {
        let block = Block {
            index: self.chain.len() as u32 + 1,
            timestamp: Utc::now().to_rfc2822(),
            proof,
            previous_hash: previous_hash.to_string(),
        };

        self.chain.push(block);
        self.chain.last().unwrap()
    }

    pub fn get_previous_block(&self) -> &Block {
        self.chain.last().unwrap()
    }

    pub fn sha256(i: i64) -> String {
        format!("{:x}", Sha256::digest(format!("{:b}", i).as_bytes()))
    }

    pub fn proof_of_work(&self, previous_proof: u32) -> u32 {
        // miners proof submitted
        let mut new_proof: u32 = 1;
        // status of proof of work
        let mut check_proof = false;
        while !check_proof {
            // problem and algorithm based off the previous proof and new proof
            let hash_operation =
                Blockchain::sha256(new_proof.pow(2) as i64 - previous_proof.pow(2) as i64);
            // check miners solution to problem, by using miners proof in cryptographic encryption
            // if miners proof results in 3 leading zero's in the hash operation, then:
            if &hash_operation[hash_operation.len() - 3..] == "000" {
                check_proof = true;
            } else {
                // if miners solution is wrong, give mine another chance until correct
                new_proof += 1;
            }
        }
        new_proof
    }

    // generate a hash of an entire block
    pub fn hash(block: &Block) -> String {
        let encoded_block = serde_json::to_string(block).unwrap();
        format!("{:x}", Sha256::digest(encoded_block.as_bytes()))
    }

    pub fn is_chain_valid(chain: &Vec<Block>) -> bool {
        // get the first block in the chain and it serves as the previous block
        let mut previous_block = match chain.get(0) {
            Some(x) => x,
            None => return true,
        };
        // an index of the blocks in the chain for iteration
        let mut block_index = 1;
        while block_index < chain.len() {
            // get the current block
            let block = chain.get(block_index).unwrap();
            // check if the current block link to previous block has is the same as
            // the hash of the previous block
            if block.previous_hash != Blockchain::hash(previous_block) {
                return false;
            }
            // run the proof data through the algorithm
            let hash_operation =
                // Blockchain::sha256(block.proof.pow(2) - previous_block.proof.pow(2));
            Blockchain::sha256(block.proof.pow(2) as i64 - previous_block.proof.pow(2) as i64);
            // check if hash operation is invalid
            if &hash_operation[hash_operation.len() - 3..] != "000" {
                return false;
            }
            // set the previous block to the current block after running validation on current block
            previous_block = block;
            block_index += 1;
        }
        true
    }
}

impl MinedResponse {
    pub fn new(message: String, block: Block) -> Self {
        MinedResponse { message, block }
    }
}

impl ChainResponse {
    pub fn new(chain: Vec<Block>) -> Self {
        ChainResponse {
            length: chain.len(),
            chain,
        }
    }
}

impl ValidateResponse {
    pub fn new(validation: bool) -> Self {
        ValidateResponse { validation }
    }
}
