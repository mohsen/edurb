extern crate mime;
extern crate serde;

use actix_web::{get, http::header, web, App, HttpResponse, HttpServer, Responder};
use edurb::{Blockchain, ChainResponse, MinedResponse, ValidateResponse};
use std::sync::Mutex;

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello miner!")
}

#[get("/mine_block")]
async fn mine_block(data: web::Data<Mutex<Blockchain>>) -> impl Responder {
    let mut blockchain = data.lock().unwrap();
    let previous_block = blockchain.get_previous_block();
    let previous_hash = Blockchain::hash(&previous_block);
    let proof = blockchain.proof_of_work(previous_block.proof());
    let block = blockchain.create_block(proof, &previous_hash);

    let response = MinedResponse::new("Block mined!".to_string(), (*block).clone());
    let response = serde_json::to_string(&response).unwrap();

    HttpResponse::Ok()
        .insert_header(header::ContentType(mime::APPLICATION_JSON))
        .body(response)
}

#[get("/get_chain")]
async fn get_chain(data: web::Data<Mutex<Blockchain>>) -> impl Responder {
    let blockchain = data.lock().unwrap();
    let response = ChainResponse::new(blockchain.chain());
    let response = serde_json::to_string(&response).unwrap();
    HttpResponse::Ok()
        .insert_header(header::ContentType(mime::APPLICATION_JSON))
        .body(response)
}

#[get("/validate_chain")]
async fn validate_chain(data: web::Data<Mutex<Blockchain>>) -> impl Responder {
    let blockchain = data.lock().unwrap();
    let response = ValidateResponse::new(Blockchain::is_chain_valid(blockchain.ref_chain()));
    let response = serde_json::to_string(&response).unwrap();
    HttpResponse::Ok()
        .insert_header(header::ContentType(mime::APPLICATION_JSON))
        .body(response)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let data = web::Data::new(Mutex::new(Blockchain::new()));

    HttpServer::new(move || {
        App::new()
            .app_data(data.clone())
            .service(hello)
            .service(mine_block)
            .service(get_chain)
            .service(validate_chain)
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
