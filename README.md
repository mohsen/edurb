## EDURB

Edurb is stand for EDUcational Rust Blockchain, a simple Blockchain (with POW consensus), written in Rust for learning.

#### To run program
Use `cargo run` then open `127.0.0.1:8080` in your browser.

<img src="./screenshots/hello_miner.png" width=400/>

use `/mine_block` path to mine block of blockchain.

<img src="./screenshots/block_mined.png" width=400/>

use `/get_chain`

<img src="./screenshots/get_chain.png" width=400/>

use `/validate_chain`

<img src="./screenshots/validation.png" width=400/>

#### TODO
Some work in progress...
